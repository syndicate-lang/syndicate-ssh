# Racket SSH server (and client)

This is a [Racket](http://racket-lang.org/) implementation of the SSH
v2 protocol. It's written to work with
[Syndicate](https://syndicate-lang.org/), but could readily be adapted
to work with other I/O substrates. (It originally used Racket's `sync`
and events directly.)

## Copyright and Licence

Copyright 2010-2021 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

This program is distributed under the terms of the [LGPLv3
license](https://opensource.org/licenses/lgpl-3.0.html) or any later version.

Documentation is distributed under the terms of the [CC BY 4.0
license](https://creativecommons.org/licenses/by/4.0/).
