#lang racket/base
;;; SPDX-License-Identifier: LGPL-3.0-or-later
;;; SPDX-FileCopyrightText: Copyright © 2012-2024 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

(require racket/set)
(require racket/match)

(provide (struct-out ssh-channel)

	 (struct-out channel-name)
	 (struct-out channel-stream-name)
	 (struct-out channel-message)

	 (struct-out channel-stream-credit)
	 (struct-out channel-stream-data)
	 (struct-out channel-stream-extended-data)
	 (struct-out channel-stream-eof)
	 (struct-out channel-stream-notify)
	 (struct-out channel-stream-request)
	 (struct-out channel-stream-ok)
	 (struct-out channel-stream-fail)
	 (struct-out channel-stream-config)
	 (struct-out channel-stream-open-failure))

;; A CloseState is one of
;; - 'neither, indicating that neither side has signalled closure
;; - 'local, only the local end has signalled closure
;; - 'remote, only the remote end has signalled closure
;; - 'both, both ends have signalled closure.
;; Represents local knowledge of the state of a shared shutdown state
;; machine.
;;
;;                'neither
;;                /       \
;;              \/         \/
;;           'local      'remote
;;               \         /
;;                \/     \/
;;                  'both

;; TODO: Once the presence-based routing table can be queried, the
;; CloseState becomes redundant, because we can just ask which roles
;; remain to decide whether either direction is still open.

;; A ChannelState is a (ssh-channel ...) TODO
;; Named ssh-channel to avoid conflicts with Racket's built-in
;; synchronous channels.
(struct ssh-channel (name ;; ChannelName
		     local-ref ;; Uint32
		     remote-ref ;; Maybe<Uint32>
		     outbound-packet-size ;; Maybe<Natural>
		     close-state ;; CloseState covering CLOSE signals
		     )
	#:transparent)

;; ChannelName = (channel-name Boolean Bytes Any)
;; Names a channel within a connection. Unique within a particular
;; connection. If (locally-originated?) is true, then the local peer
;; is the one that opened this channel, and the local peer is
;; reponsible for choosing the (identifier) and ensuring that it is
;; unique with respect to other locally-originated streams within this
;; connection; if false, the remote peer opened the channel, and the
;; (identifier) is chosen managed by the connection-control code. If
;; (locally-originated?) is true, the (type) is chosen by the local
;; peer, otherwise it is chosen by the remote peer.
(struct channel-name (locally-originated? type identifier) #:prefab)

;; ChannelStreamName = (channel-stream-name Boolean ChannelName)
;; Names a stream within a channel within a connection. If (inbound?)
;; is true, this is the stream of packets from the remote peer to the
;; local peer; if false, the reverse.
(struct channel-stream-name (inbound? channel) #:prefab)

;; ChannelMessage = (channel-message ChannelStreamName ChannelMessageBody)
;; Relates a message to a particular stream within a channel within a
;; connection.
(struct channel-message (stream-name body) #:prefab)

;; A ChannelMessageBody is one of
;; -- (channel-stream-credit NonNegativeInteger) **
;;    Informs the publisher that it may transmit another (count)
;;    bytes.
;; -- (channel-stream-data Bytes)
;;    Data intended for the subscriber.
;; -- (channel-stream-extended-data Uint32 Bytes)
;;    Extended data intended for the subscriber. The type code is one
;;    of those defined in ssh-numbers.rkt; for example,
;;    SSH_EXTENDED_DATA_STDERR.
;; -- (channel-stream-eof)
;;    Signals the end of the data stream. Notice that channel closure
;;    is signalled with presence changes.
;; -- (channel-stream-notify Bytes Bytes)
;;    One-way notification of SSH_MSG_CHANNEL_REQUEST.
;; -- (channel-stream-request Bytes Bytes)
;;    RPC SSH_MSG_CHANNEL_REQUEST request.
;; -- (channel-stream-ok) **
;;    RPC SSH_MSG_CHANNEL_REQUEST reply.
;; -- (channel-stream-fail) **
;;    RPC SSH_MSG_CHANNEL_REQUEST error.
;; -- (channel-stream-config Uint32 Bytes) **
;;    Either SSH_MSG_CHANNEL_OPEN or
;;    SSH_MSG_CHANNEL_OPEN_CONFIRMATION, depending on direction of
;;    travel. Must be sent before any channel-stream-credit messages.
;; -- (channel-stream-open-failure Uint32 Bytes)
;;    SSH_MSG_CHANNEL_OPEN_FAILURE.
;;
;; Messages marked ** travel "upstream", from subscriber to publisher.
(struct channel-stream-credit (count) #:prefab)
(struct channel-stream-data (bytes) #:prefab)
(struct channel-stream-extended-data (type bytes) #:prefab)
(struct channel-stream-eof () #:prefab)
(struct channel-stream-notify (type bytes) #:prefab)
(struct channel-stream-request (type bytes) #:prefab)
(struct channel-stream-ok () #:prefab)
(struct channel-stream-fail () #:prefab)
(struct channel-stream-config (maximum-packet-size extra-data) #:prefab)
(struct channel-stream-open-failure (reason description) #:prefab)
