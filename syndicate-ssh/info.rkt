;;; SPDX-License-Identifier: LGPL-3.0-or-later
;;; SPDX-FileCopyrightText: Copyright © 2021-2024 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

#lang setup/infotab
(define collection "syndicate-ssh")

(define deps '(

	       "base"

               "bitsyntax"
               "crypto-lib"
               "preserves"
               "syndicate"

               "unix-socket-lib"
               "sandbox-lib"

               ))

(define build-deps '("rackunit-lib"))

(define pre-install-collection "private/install.rkt")
